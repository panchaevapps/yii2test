<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use app\models\Orders;
use yii\grid\GridView;

$this->title = 'Список заказов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">

<?php


    echo $this->render('_search', ['model' => $searchModel]);

    echo GridView::widget([
            'dataProvider' => $dataProvider,

            // инлайн поиск по полям
            //    'filterModel' => $searchModel,

            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                ['attribute' => 'id'],
                ['attribute' => 'seller_id', 'label' => 'Продавец', 'value' => 'seller.title'],
                ['attribute' => 'date_sold'],
                ['attribute' => 'qty'],
                ['attribute' => 'order_sum'],

                // скрываем столбец кнопок редактирования строки, а сам набор кнопок меняем, чтоб было
                ['class' => \yii\grid\ActionColumn::class, 'template' => '{delete} {view}',
                    'visible' => false,]
            ]
        ]
    );

    ?>
</div>
