<?php

use app\models\Sellers;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\MyOrdersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="my-orders-search">
<div class="row">
    <div class="col-xs-6">
    <?php $form = ActiveForm::begin([
        'action' => ['about'],
        'method' => 'get',
    ]);
    ?>

    <?
    $arrSellers = ArrayHelper::map(Sellers::find()->all(), 'id', 'title');
    array_unshift($arrSellers, 'все');

    echo $form->field($model, 'seller_id')
        ->dropDownList($arrSellers);
    ?>

    <?= $form->field($model, 'date_sold_from')->widget(DatePicker::class, [
        'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
        'options' => [
            'placeholder' => Yii::$app->formatter->asDate($model->date_sold_from),
            'class'=> 'form-control',
            'autocomplete'=>'off'
        ],
        'clientOptions' => [
            'changeMonth' => true,
            'changeYear' => true,
            'yearRange' => '2000:2050',
            'showOn' => 'button',
            'buttonText' => 'Начальная дата',
            //'buttonImageOnly' => true,
            //'buttonImage' => 'images/calendar.gif'
        ]])->label(false) ?>

    <?= $form->field($model,'date_sold_to')->widget(DatePicker::class, [
    'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    'options' => [
    'placeholder' => Yii::$app->formatter->asDate($model->date_sold_to),
    'class'=> 'form-control',
    'autocomplete'=>'off'
    ],
    'clientOptions' => [
    'changeMonth' => true,
    'changeYear' => true,
    'yearRange' => '2000:2050',
    'showOn' => 'button',
    'buttonText' => 'Конечная дата',
    //'buttonImageOnly' => true,
    //'buttonImage' => 'images/calendar.gif'
    ]])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton('Искать', ['class' => 'btn btn-primary', "name"=>"exportBtn", "value"=>"0"]) ?>
        <?= Html::submitButton('Создать отчёт в Excel', ['class' => 'btn btn-primary', "name"=>"exportBtn", "value"=>"1"]) ?>

    </div>

    <?php ActiveForm::end(); ?>

        <?php $form = ActiveForm::begin([
            'action' => ['about'],
            'method' => 'get',
        ]); ?>

        <div class="form-group">
            <?= Html::submitButton('Отменить фильтр', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>

