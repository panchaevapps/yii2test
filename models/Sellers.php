<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sellers".
 *
 * @property int $id
 * @property string|null $title
 */
class Sellers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sellers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }

    public function getOrders() {
        return $this->hasMany(Orders::class, ['seller_id' => 'id']);
    }
}
