<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class Orders extends ActiveRecord {

//    public $id;
//    public $date_sold;
//    public $seller_id;
//    public $qty;
//    public $order_sum;

    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['seller_id', 'qty', 'order_sum'], 'default', 'value' => null],
            [['seller_id', 'qty', 'order_sum'], 'integer'],
            [['seller_name'], 'string'],
            [['date_sold', 'date_sold_from', 'date_sold_to', 'seller_name', 'seller_id'], 'safe'],
        ];
    }

    /**
     * @return string[]
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'seller_id' => 'ID продавца',
            'date_sold' => 'Дата и время продажи',
            'qty' => 'Количество',
            'order_sum' => 'Сумма заказа (руб)',
            'seller_name' => 'Продавец',
            'date_sold_from' => 'Начальная дата',
            'date_sold_to' => 'Конечная дата',


        ];
    }



    /**
     */
    public function getSeller() {
        return
            $this->hasOne(Sellers::class, ['id' => 'seller_id']);
    }
}

