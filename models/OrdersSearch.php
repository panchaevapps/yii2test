<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class OrdersSearch extends Orders
{
    public $seller_name, $seller_id;
    public $date_sold_from, $date_sold_to;

    public function rules()
    {
        // только поля определенные в rules() будут доступны для поиска
        return [
            [['date_sold'], 'string'],
            [['seller_id', 'id'], 'integer'],

            [['date_sold', 'date_sold_from', 'date_sold_to', 'seller_name', 'seller_id', 'id'], 'safe'],

        ];
    }

    public function attributeLabels()
    {
        return [
            'seller_name' => 'Продавец',
            'date_sold_from' => 'Начальная дата',
            'date_sold_to' => 'Конечная дата',
            'seller_id' => 'Продавец',
        ];
    }
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Orders::find();
        $query->joinWith('seller');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],

        ]);

        // получаем все данные
        if (!($this->load($params) && $this->validate())) {

            return $dataProvider;
        }

        $isDateFilter = false;

        // если границы периода некорректны
        if ( isset($this->date_sold_to) &&
            isset($this->date_sold_from) &&
            $this->date_sold_to >= $this->date_sold_from
            ){

            $isDateFilter = true;


        }

        if ($isDateFilter) {
            // детализируем по периоду времени
            $query->andFilterWhere(['>=', 'date_sold', $this->date_sold_from])
                ->andFilterWhere(['<=', 'date_sold', $this->date_sold_to]);
        }


        // детализируем по продавцу
        if ($this->seller_id > 0) {
            $query->andFilterWhere(['=', 'seller_id', $this->seller_id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $dataProvider;
    }

}
