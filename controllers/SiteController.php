<?php

namespace app\controllers;

use app\models\Sellers;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Orders;
use app\models\OrdersSearch;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {

        $params = Yii::$app->request->queryParams;
        $searchModel = new OrdersSearch();

        $dataProvider = $searchModel->search($params);
//print_r($params);
        if ( $params['exportBtn'] == 1) {

            $this->exportExcel($params);
            die();
        }



        return $this->render('about', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);


    }

    function exportExcel( $params) {

        $params = Yii::$app->request->queryParams['OrdersSearch'];

        $query = new \yii\db\Query();

        $query->select(
            ['orders.seller_id',
                'orders.id', 'orders.date_sold', 'orders.qty','orders.order_sum','sellers.title AS seller_title'] )
            ->from('orders');

        $query->leftJoin( 'sellers', 'sellers.id=orders.seller_id');

        // фильтр по продавцу, в этом сценарии не используем

        if ($params['seller_id'] > 0) {
            $query->where('orders.seller_id='.$params['seller_id'] );
        }


        // детализируем по периоду времени
        if ( isset($params['date_sold_to']) &&
            !empty($params['date_sold_from']) &&
            !empty($params['date_sold_to']) &&
            isset($params['date_sold_from']) ) {


            if ($params['date_sold_to'] < $params['date_sold_from']) {
                $params['date_sold_to'] = $params['date_sold_from'];
            }

            $query->andWhere("orders.date_sold>='{$params['date_sold_from']}'" );
            $query->andWhere("orders.date_sold<='{$params['date_sold_to']}'");

            $query->limit(1000000)->offset(0);
        }

        $query->orderBy(['date_sold' => SORT_ASC, 'seller_title' => SORT_ASC]);
        $command = $query->createCommand();

        $rows = $query->all();
        $count =$query->count();

        // HTTP-заголовки
        header ("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
        header ("Last-Modified: " . gmdate("D,d M Y H:i:s") . " GMT");
        header ("Cache-Control: no-cache, must-revalidate");
        header ("Pragma: no-cache");
        header ("Content-type: application/vnd.ms-excel");
        header ("Content-Disposition: attachment; filename=sales-report-".date('Ymd_Hmi', time()).".xls");

        // TODO парсинг данных из $dataProvider
        //

        // TODO формирование листа excel по полученным данным

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Sales report');

        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');

        die;


    }

    protected function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
